public class Rectangulo extends FiguraGeometrica{

private double base;
private double altura;


public Rectangulo(double b, double a){
 base = b;
 altura = a;
}

public double area(){
 return base * altura;
}

public double perimetro(){
return 2*(base +altura);
}

}
