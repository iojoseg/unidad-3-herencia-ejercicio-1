public class Principal{
 public static void main(String args[]){
 
 Rectangulo rect = new Rectangulo(3.0,7.0);
 Circulo circ = new Circulo(5.0);
 
 rect.setColor("azul");
 circ.setColor("verde");
 
 System.out.println("El Rectangulo es de color: " + rect.getColor());
 System.out.println("El area del rectangulo es: " + rect.area());
 System.out.println("El perimetro del rectangulo es: " + rect.perimetro());
 System.out.println("");
 System.out.println("El Circulo  es de color: " + circ.getColor());
 System.out.println("El area del rectangulo es: " + circ.area());
 System.out.println("El perimetro del rectangulo es: " + circ.perimetro());
 
 
 }
}